package GuerraDeRectángulos;

import java.util.ArrayList;
import java.util.Random;

public class Tablero {
	
	private Integer[][] tablero;
	private ArrayList<Rectangulo> rectsJ1;
	private ArrayList<Rectangulo> rectsJ2;	
	private int turno;
	private Rectangulo ultRect;
	private Random random = new Random();

	public Tablero(Integer ancho, Integer largo, int turno){
		
		iniciarTablero(ancho, largo);			
		iniciarRects();
		iniciarTurnos(turno);
		
	}

	private void iniciarTurnos(int turno) {
		this.turno =1;
	}

	private void iniciarRects() {
		this.rectsJ1 = new ArrayList<Rectangulo>();
		this.rectsJ2 = new ArrayList<Rectangulo>();
		this.ultRect = null;
	}

	private void iniciarTablero(Integer ancho, Integer largo) {
		this.tablero = new Integer[largo][ancho];
		for (int fila=0; fila<largo;fila++){
			for (int columna=0; columna<ancho;columna++){
				tablero[fila][columna]=0;
			}
		}
	}
	
	public Integer agregarRect(Integer ancho, Integer largo, Integer jugador){
		ArrayList<Rectangulo> posibles = rectsPosibles(ancho,largo,jugador);
		for (int i=0; i<posibles.size();i++){
			if (posibles.get(i).getJugador()==1){	
				if (estaVacio(posibles.get(i))==true){
					if (estaAdyacente(posibles.get(i))==true || rectsJ1.size()==0){	
						for (int fila=posibles.get(i).getEjeX(); fila<posibles.get(i).getEjeX()+posibles.get(i).getLargo();fila++){
							for (int columna=posibles.get(i).getEjeY(); columna<posibles.get(i).getEjeY()+posibles.get(i).getAncho();columna++){
								if (tablero[fila][columna]==0){
									tablero[fila][columna]=posibles.get(i).getJugador();	
								}							
							}
						}
						rectsJ1.add(posibles.get(i));
						ultRect = posibles.get(i);
						return 0;
				}}
			}else{
				if (estaVacio(posibles.get(i))==true && (estaAdyacente(posibles.get(i))==true) || rectsJ2.size()==0){
					for (int fila=posibles.get(i).getEjeX(); fila<posibles.get(i).getEjeX()+posibles.get(i).getLargo();fila++){
						for (int columna=posibles.get(i).getEjeY(); columna<posibles.get(i).getEjeY()+posibles.get(i).getAncho();columna++){
							tablero[fila][columna]=posibles.get(i).getJugador();						
						}
					}
					rectsJ2.add(posibles.get(i));
					ultRect = posibles.get(i);
					return 0;
				}
			}
		}
		return 1;		
	}
	
	private ArrayList<Rectangulo> rectsPosibles(Integer ancho, Integer largo, Integer jugador){
		ArrayList<Rectangulo> posibles = new ArrayList<Rectangulo>();
		for (int fila=0; fila<tablero.length-largo;fila++){
			for (int columna=0; columna<tablero[0].length-ancho;columna++){				
				posibles.add(new Rectangulo(ancho,largo,fila,columna,jugador));
			}
		}		
		return posibles;
	}
	
	boolean estaVacio(Rectangulo aMeter){
		for (int fila=aMeter.getEjeX(); fila<aMeter.getEjeX()+aMeter.getLargo();fila++){
			for (int columna=aMeter.getEjeY(); columna<aMeter.getEjeY()+aMeter.getAncho();columna++){
				if (tablero[fila][columna]==1 || tablero[fila][columna]==2){
					return false;
				}				
			}
		}
		return true;
	}
	
	boolean estaAdyacente(Rectangulo aMeter){
		boolean ret = false;
		for (int fila=aMeter.getEjeX(); fila<aMeter.getEjeX()+aMeter.getLargo();fila++){
			for (int columna=aMeter.getEjeY(); columna<aMeter.getEjeY()+aMeter.getAncho();columna++){
				if (fila==0 && columna>0 && columna<this.tablero[0].length){
					ret = ret || tablero[fila+1][columna]==aMeter.getJugador();
					ret = ret || tablero[fila][columna+1]==aMeter.getJugador();
					ret = ret || tablero[fila][columna-1]==aMeter.getJugador();
				}else if (fila>0 && columna==0 && fila<this.tablero.length){
					ret = ret || tablero[fila-1][columna]==aMeter.getJugador();
					ret = ret || tablero[fila+1][columna]==aMeter.getJugador();
					ret = ret || tablero[fila][columna+1]==aMeter.getJugador();
				}else if (fila==0 && columna==0){
					ret = ret || tablero[fila+1][columna]==aMeter.getJugador();
					ret = ret || tablero[fila][columna+1]==aMeter.getJugador();
				}else if (fila>0 && columna>0  && columna<this.tablero[0].length && fila<this.tablero.length){
					ret = ret || tablero[fila+1][columna]==aMeter.getJugador();
					ret = ret || tablero[fila][columna+1]==aMeter.getJugador();
					ret = ret || tablero[fila-1][columna]==aMeter.getJugador();
					ret = ret || tablero[fila][columna-1]==aMeter.getJugador();
				}else{					
				}	
				if (ret==true){
					return ret;
				}
			}if (ret==true){
				return ret;
			}
		}		
		return ret;
	}
	
	public int eliminarRect(){
		if (turno==1){
			if (rectsJ2.size()!=0){
				Rectangulo eliminado = rectsJ2.get(random.nextInt(rectsJ2.size()));
				for (int fila=eliminado.getEjeX(); fila<eliminado.getEjeX()+eliminado.getLargo();fila++){
					for (int columna=eliminado.getEjeY(); columna<eliminado.getEjeY()+eliminado.getAncho();columna++){
						tablero[fila][columna]=0;
					}
				}
				rectsJ2.remove(eliminado);
				return 2;
			}			
		}else{
			if (rectsJ1.size()!=0){
				Rectangulo eliminado = rectsJ1.get(random.nextInt(rectsJ1.size()));
				for (int fila=eliminado.getEjeX(); fila<eliminado.getEjeX()+eliminado.getLargo();fila++){
					for (int columna=eliminado.getEjeY(); columna<eliminado.getEjeY()+eliminado.getAncho();columna++){
						tablero[fila][columna]=0;
					}
				}
				rectsJ1.remove(eliminado);
				return 1;
			}
		}
		return turno;
	}

	public Integer area(Integer jugador){
		int area=0;
		if (jugador==1){
			for (int i=0; i<rectsJ1.size();i++){
				area+=rectsJ1.get(i).getAncho()*rectsJ1.get(i).getLargo();
			}
		}else{
			for (int i=0; i<rectsJ2.size();i++){
				area+=rectsJ2.get(i).getAncho()*rectsJ2.get(i).getLargo();
			}
		}
		return area;
	}
	
	public Rectangulo ultimoRectangulo(){	
		if (this.ultRect!=null){
			return this.ultRect;
		}
		return new Rectangulo(0,0,0,0,0);
	}
	
	public boolean equals(Tablero otro){
		boolean ret = true;
		if (tablero.length == otro.tablero.length && tablero[0].length==otro.tablero[0].length){
			for(int fila=0;fila<tablero.length;fila++){
				for(int columna=0;columna<tablero[0].length;columna++){
					ret = ret && tablero[fila][columna]==otro.tablero[fila][columna];
				}
			}
		}else{
			return false;
		}		
		if (rectsJ1.size()==otro.rectsJ1.size() && rectsJ2.size()==otro.rectsJ2.size() && ret==true){
			for(int i=0;i<rectsJ1.size();i++){
				ret = ret && rectsJ1.get(i).equals(otro.rectsJ1.get(i));
			}
			for(int i=0;i<rectsJ1.size();i++){
				ret = ret && rectsJ1.get(i).equals(otro.rectsJ1.get(i));
			}
		}else{
			return false;
		}		
		ret = ret && this.area(1)==otro.area(1) && this.area(2)==otro.area(2);	
		return ret;
	}
	
	public ArrayList<Rectangulo> getRectsJ1() {
		return rectsJ1;
	}

	public void setRectsJ1(ArrayList<Rectangulo> rectsJ1) {
		this.rectsJ1 = rectsJ1;
	}

	public ArrayList<Rectangulo> getRectsJ2() {
		return rectsJ2;
	}

	public void setRectsJ2(ArrayList<Rectangulo> rectsJ2) {
		this.rectsJ2 = rectsJ2;
	}
	
	public Integer[][] getTablero() {
		return this.tablero;
	}
	
	public void getTablero(Integer[][] tablero) {
		this.tablero = tablero;
	}
	
	public void setTurno(int x){
		this.turno=x;
	}
	
	@Override
	public String toString() {
		StringBuilder mostrarTablero = new StringBuilder();
		for(int fila=0;fila<tablero.length;fila++){
			for(int columna=0;columna<tablero[0].length;columna++){
				mostrarTablero.append(this.tablero[fila][columna]);
				if (columna!=tablero[0].length-1){
					mostrarTablero.append(" - ");
				}				
			}
			mostrarTablero.append("\n");
		}
		return mostrarTablero.toString();
	}

}
