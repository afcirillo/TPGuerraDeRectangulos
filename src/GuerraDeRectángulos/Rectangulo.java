package GuerraDeRectángulos;
public class Rectangulo {
	
	private Integer ancho;
	private Integer largo;
	private Integer ejeX;
	private Integer ejeY;
	private Integer jugador;	
	
	Rectangulo(Integer ancho, Integer largo, Integer ejeX, Integer ejeY, Integer jugador){
		this.ancho = ancho;
		this.largo = largo;
		this.ejeX = ejeX;
		this.ejeY = ejeY;
		this.jugador = jugador;
	}
	
	Integer area(){
		return ancho*largo;
	}

	public Integer getAncho() {
		return ancho;
	}

	public void setAncho(Integer ancho) {
		this.ancho = ancho;
	}

	public Integer getLargo() {
		return largo;
	}

	public void setLargo(Integer largo) {
		this.largo = largo;
	}

	public Integer getEjeX() {
		return ejeX;
	}

	public void setEjeX(Integer ejeX) {
		this.ejeX = ejeX;
	}

	public Integer getEjeY() {
		return ejeY;
	}

	public void setEjeY(Integer ejeY) {
		this.ejeY = ejeY;
	}

	public Integer getJugador() {
		return jugador;
	}

	public void setJugador(Integer jugador) {
		this.jugador = jugador;
	}
	
	public boolean equals(Rectangulo otro){
		if (this.ancho == otro.ancho && this.largo == otro.largo && 
			this.ejeX == otro.ejeX && this.ejeY == otro.ejeY && this.jugador == otro.jugador){
				return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		StringBuilder mostrarRect = new StringBuilder();
		mostrarRect.append("Rectángulo: " + "Ancho = " + this.ancho + " - Largo = " + this.largo
		+ " - Margen Superior Izquierdo = (" + this.ejeX + ", " + this.ejeY + ") - Jugador = " + this.jugador + "\n");
		
		return mostrarRect.toString();
	}

}
