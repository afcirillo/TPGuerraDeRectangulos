package GuerraDeRectángulos;
import org.junit.Test;

public class GRMainTest {

	//--------------Clase hecha para probar los main del pdf------------------
	
	@Test
	public void mainTest() {
		System.out.println("MainTest= ");
		GR gr = new GR(21,20);
		String ganador = gr.jugar();
		while (ganador == "") {
			ganador = gr.jugar();
			System.out.println("ultRect ancho = " + gr.ultimoRectangulo().getAncho() +", ultRect largo =  "+ gr.ultimoRectangulo().getLargo());
		}
		System.out.println(gr);
	}
	
	@Test
	public void mainTest2() {
		System.out.println("MainTest2= ");
		GR gr2 = new GR(21,22);
		String ganador="";
		ganador = gr2.jugar();
		ganador = gr2.jugar();
		ganador = gr2.jugar();
		gr2.eliminarRect();
		System.out.println(gr2);
		}

}
