package GuerraDeRectángulos;
import java.util.ArrayList;
import java.util.Random;

public class GR {
	
	Tablero tablero;
	private int turno;
	private ArrayList<Integer> perder;
	private String ganador;
	private Random random = new Random();

	public GR(Integer ancho, Integer largo){
		
		iniciarValores();
		iniciarTablero(ancho, largo);			
		
	}

	private void iniciarValores() {
		this.turno =1;
		this.perder = new ArrayList<Integer>();
		this.ganador = "";
	}

	private void iniciarTablero(Integer ancho, Integer largo) {
		this.tablero = new Tablero(ancho,largo, turno);
	}
	
	public Integer agregarRect(Integer ancho, Integer largo, Integer jugador){
		return tablero.agregarRect(ancho, largo, jugador);
	}

	
	public void eliminarRect(){
		tablero.setTurno(turno);
		turno=tablero.eliminarRect();		
	}
	
	public String jugar(){
		perder.add(agregarRect(random.nextInt(6)+1, random.nextInt(6)+1, turno));
		if (perder.size()>1){
			if (perder.get(perder.size()-1)==1 && perder.get(perder.size()-2)==1){
				ganador = "Jugador "+finDelJuego();			
				return ganador;
			}
		}
		if (turno==1){
			turno=2;
		}else{
			turno=1;
		}			
		return ganador;
	}
	
	public String jugar(Integer ancho, Integer largo){
		perder.add(agregarRect(ancho,largo, turno));
		if (perder.size()>1){
			if (perder.get(perder.size()-2)==1 && perder.get(perder.size()-1)==1){
				ganador = "Jugador "+finDelJuego();			
				return ganador;
			}
		}
		if (turno==1){
			turno=2;
		}else{
			turno=1;
		}	
		return ganador;
	}
	
	private Integer finDelJuego() {
		int ganador = 0;
		if (area(1)>area(2)){
			ganador = 1;
		}else{
			ganador = 2;	
		}
		return ganador;
	}

	public Integer area(Integer jugador){
		return tablero.area(jugador);
	}
	
	public Rectangulo ultimoRectangulo(){		
		return tablero.ultimoRectangulo();
	}
	
	public void setTurno(int x){
		this.turno = x;
	}
	
	public boolean equals(GR otro){
		return tablero.equals(otro.tablero);		
	}
	
	@Override
	public String toString() {
		StringBuilder mostrarTablero = new StringBuilder();
		mostrarTablero.append(tablero.toString());
		return mostrarTablero.toString();
	}

}
