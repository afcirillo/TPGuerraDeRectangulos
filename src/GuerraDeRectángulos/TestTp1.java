package GuerraDeRect�ngulos;
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.Before;

public class TestTp1 {
	private GR gr1, gr2, gr3;
	@Before
	public void setUp() {
		gr1 = new GR(11,10);
		gr2 = new GR(11,11);
		gr3 = new GR(11,10);
	}
	
	@Test
	public void testEquals1() {
		//gr1 tiene que ser igual que gr3 y distinto que gr2
		assertTrue(gr1.equals(gr3));
		assertTrue(!gr1.equals(gr2));
	}
	
	@Test
	public void testEquals2() {
		gr1.jugar(2,3);
		gr3.jugar(2,3);
		gr2.jugar(3,2);
		//gr1 tiene que ser igual que gr3 y distinto que gr2
		assertTrue(gr1.equals(gr3));
		assertTrue(!gr1.equals(gr2));
	}
	
	@Test
	public void testArea() {
		gr1.jugar();
		Rectangulo h1 = gr1.ultimoRectangulo();
		gr1.jugar();
		Rectangulo h2 = gr1.ultimoRectangulo();
		assertEquals(gr1.area(1)+gr1.area(2),h1.area()+h2.area());
	}
	
	@Test
	public void testEliminar() {
		int areaini = gr1.area(1) + gr1.area(2); assertEquals(0,areaini); //�rea inicial 0
		gr1.jugar(); //turno Jug.1 agrega un rect�ngulo 
		int area1 = gr1.area(1); 
		int area = gr1.area(1) + gr1.area(2);		
		gr1.jugar(); //turno Jug.2 agrega un rect�ngulo
		gr1.eliminarRect(); //turno Jug.1 � elimina el �nico rect�ngulo del Jug.2
		int area2 = gr1.area(2);
		assertTrue(area2<=area1);
		// queda el �rea total anterior al turno del jugador 2
		// se elimina uno al azar pero hay uno solo para eliminar.
		assertEquals(gr1.area(1) + gr1.area(2), area);
	}
	
	//Nuevos Test...
	
	@Test
	public void estaVacioTest() {
		GR t1 = new GR(8,9);
		assertTrue(t1.tablero.getTablero()[1][1]==0);		
		Rectangulo r1 = new Rectangulo(5,4,2,2,1);
		assertTrue(t1.tablero.estaVacio(r1)==true);
		t1.tablero.getTablero()[2][4]=1;
		assertTrue(t1.tablero.estaVacio(r1)==false);
		
	}
	
	@Test
	public void estaAdyacenteTest() {
		GR t1 = new GR(20,20);		
		Rectangulo r1 = new Rectangulo(2,4,4,1,1);
		t1.agregarRect(5,4,1);
		assertTrue(t1.tablero.estaAdyacente(r1)==true);
	}
	
	@Test
	public void agregarTest() {
		GR t1 = new GR(12,12);	
		t1.agregarRect(5, 4, 2);
		t1.agregarRect(2, 3, 1);
		assertTrue(t1.tablero.getTablero()[0][0]==2);
		assertTrue(t1.tablero.getTablero()[0][5]==1);
	}
	
	@Test
	public void areaTest() {
		GR t1 = new GR(12,12);	
		t1.agregarRect(5, 4, 2);
		assertTrue(t1.area(2)==20);
		t1.agregarRect(2, 3, 1);
		assertTrue(t1.area(1)==6);
		t1.agregarRect(4, 3, 2);
		t1.agregarRect(2, 3, 1);
		t1.agregarRect(1, 3, 1);
		assertTrue(t1.area(1)<t1.area(2));
	}
	
	@Test
	public void eliminarRectTest(){
		GR t1 = new GR(12,12);	
		t1.agregarRect(5, 4, 2);
		t1.setTurno(1);
		t1.eliminarRect();
		assertTrue(t1.area(2)==0);
		t1.agregarRect(3, 4, 1);
		t1.setTurno(2);
		t1.eliminarRect();
		assertTrue(t1.area(1)==0);
	}
	
	@Test
	public void ultimoRectanguloTest(){
		GR t1 = new GR(12,12);	
		t1.agregarRect(5, 4, 2);
		Rectangulo r1 = t1.ultimoRectangulo();
		t1.agregarRect(2, 3, 1);
		Rectangulo r2 = t1.ultimoRectangulo();
		assertFalse(r1.equals(r2));
	}

}
